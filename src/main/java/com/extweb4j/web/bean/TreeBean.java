package com.extweb4j.web.bean;

import java.util.ArrayList;
import java.util.List;

public class TreeBean {
	
	public String id;
	private String text;
	private boolean  checked = false;
	private boolean expanded = false;
	private boolean leaf = true;
	private List<TreeBean> children = new ArrayList<TreeBean>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	public boolean isLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	public List<TreeBean> getChildren() {
		return children;
	}
	public void setChildren(List<TreeBean> children) {
		this.children = children;
	}
	public TreeBean(String id, String text, boolean checked, boolean expanded,
			boolean leaf, List<TreeBean> children) {
		super();
		this.id = id;
		this.text = text;
		this.checked = checked;
		this.expanded = expanded;
		this.leaf = leaf;
		this.children = children;
	}
	public TreeBean(String id, String text, boolean checked, boolean expanded,
			boolean leaf) {
		super();
		this.id = id;
		this.text = text;
		this.checked = checked;
		this.expanded = expanded;
		this.leaf = leaf;
	}
	public TreeBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
