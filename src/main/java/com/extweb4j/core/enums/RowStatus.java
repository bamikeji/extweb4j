package com.extweb4j.core.enums;
/**
 * 行数据状态标志
 * @author Administrator
 *
 */
public enum  RowStatus {
	
	启用("0"), 禁用("1"),删除("2");
	
	private String state = "0";

	private RowStatus(String state) {
		this.state = state;
	}

	public String  getState() {
		return state;
	}
}
