package com.extweb4j.core.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.extweb4j.core.anno.AuthAnno;
import com.extweb4j.core.bean.Msg;
import com.extweb4j.core.comstant.CacheConstant;
import com.extweb4j.core.daoaload.ActionAuthDataLoader;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.model.ExtMenu;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheKit;

public class ExtInterceptor implements Interceptor {

	static Logger LOG = Logger.getLogger(ExtInterceptor.class);
	
	static final String NO_AUTH = "您没有权限";
	static final String LOGIN_ERROR = "您的会话已过期,请重新登录";
	
	@Override
	public void intercept(Invocation in) {
		// TODO Auto-generated method stub
		Controller c = in.getController();
		
		// 1.验证权限
		{
			String uid = c.getSessionAttr("uid");
			String action = c.getRequest().getRequestURI();
			
			if(uid==null){
				c.renderJson(new Msg(false, LOGIN_ERROR));
				return;
			}
			
			List<String> auths = new ArrayList<String>();
			List<ExtMenu> list = CacheKit.get(CacheConstant.GLOBAL_CACHE_NAME,(uid+"_action"), new ActionAuthDataLoader(uid));
			for(ExtMenu m : list){
				auths.add(m.getStr("action"));
			}
			
			if(in.getMethod().getAnnotation(AuthAnno.class) != null){
				if(!auths.contains(action)){
					LOG.error(NO_AUTH);
					c.renderJson(new Msg(false, NO_AUTH));
					return;
				}
			}
			
		}
		
		// 2.执行Action
		try {
			in.invoke();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			c.renderJson(new Msg(false, ExtKit.formatException(e)));
		}
	}

	
}
