package com.extweb4j.core.model;

import java.util.List;
/**
 * 用户角色关系
 * @author Administrator
 *
 */
public class ExtUserRole extends CoreModel<ExtUserRole>{

	
	private static final long serialVersionUID = 1L;

	public static ExtUserRole dao = new ExtUserRole();

	public List<ExtUserRole> findRoleByUserId(String userId) {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer();
		buffer.append(" SELECT ur.id,r.role_name FROM ext_user_role ur ");
		buffer.append(" LEFT JOIN ext_role r ON r.id = ur.role_id");
		buffer.append(" WHERE ur.user_id = ?");
		return ExtUserRole.dao.find(buffer.toString(),userId);
	}
}
