package com.extweb4j.core.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.extweb4j.core.enums.RowStatus;
import com.extweb4j.core.kit.ExtKit;
import com.jfinal.plugin.activerecord.Page;
/**
 * 用户
 * @author Administrator
 *
 */
public class ExtUser extends CoreModel<ExtUser>{

	
	private static final long serialVersionUID = 1L;

	public static ExtUser dao = new ExtUser();

	public boolean isExist(String str) {
		// TODO Auto-generated method stub
		return super.findBy("user_id = ?", str) == null;
	}

	public Page<ExtUser> pageUserBy(int page, int limit, String keywords) {
		// TODO Auto-generated method stub
		
		String select  =  "SELECT u.id,u.user_id,u.user_pwd,u.user_name,u.user_email,"
				+ "u.row_status,u.create_time,u.user_desc,d.dept_name,d.id AS dept_id,d.dept_desc ";
		
		StringBuffer form = new StringBuffer(" FROM ext_user u  ");
		form.append(" LEFT JOIN  ext_dept d ON d.id = u.dept_id");
		
		List<Object> paras = new ArrayList<Object>();
		
		form.append(" WHERE row_status != ? ");
		paras.add(RowStatus.删除.getState());
		
		if(StringUtils.isNotBlank(keywords)){
			keywords = ExtKit.sqlFilterValidate(keywords);
			form.append(" AND (u.user_id LIKE '%"+keywords+"%' OR u.user_name LIKE '%"+keywords+"%' )");
		}
		form.append(" ORDER BY u.create_time DESC");
		return super.paginate(page, limit, select, form.toString(), paras.toArray());
	}
	/**
	 * 登录
	 * @param userId
	 * @param password
	 * @return
	 */
	public ExtUser findUserBy(String userId, String password) {
		// TODO Auto-generated method stub
		return ExtUser.dao.findFirst("SELECT * FROM ext_user u WHERE u.user_id = ? AND u.user_pwd = ?",userId,password);
	}
	
	public String getId(){
		return this.getStr("id");
	}
	public String getLoginId(){
		return this.getStr("user_id");
	}
}
