Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',
    
    name: 'Admin',
	
    defaultToken : 'dashboard',

    mainView: 'Admin.view.main.Main',
    
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },
    
    launch : function(){
    	
    }
});
