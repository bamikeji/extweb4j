Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    //TODO: implement central Facebook OATH handling here

    onFaceBookLogin : function() {
        this.redirectTo('dashboard', true);
    },
    /**
     * 登录
     */
    onLoginButton: function(b,opts) {
    	
    	var me = this;
    	var form = b.up("form").getForm();
    	var viewModel = me.getViewModel();
    	form.submit({
			waitMsg:'登录中...',
			clientValidation: true,
			url:'/doLogin',
			params:form.getFieldValues(),
			success:function(form,action){
				me.redirectTo('dashboard', true);
				//加载菜单
				var treeStore = Ext.getCmp('navigationTreeListId').getStore();
				if(treeStore && treeStore.getCount()==0){
					treeStore.reload();
				}
			},
			failure:function(form,action){
				switch (action.failureType) {
					case Ext.form.action.Action.CLIENT_INVALID:
						Ext.create('Admin.ux.Action').error('客户端验证不通过');
						break;
					default:
						viewModel.set("error",action.result.msg+"!");
				 }
			   }	
			});
    },

    onLoginAsButton: function() {
        this.redirectTo('login', true);
    },

    onNewAccount:  function() {
        this.redirectTo('register', true);
    },

    onSignupClick:  function() {
        this.redirectTo('dashboard', true);
    },

    onResetClick:  function() {
        this.redirectTo('dashboard', true);
    }
});