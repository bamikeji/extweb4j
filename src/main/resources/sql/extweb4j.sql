/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : extweb4j

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-07-11 12:50:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ext_dept`
-- ----------------------------
DROP TABLE IF EXISTS `ext_dept`;
CREATE TABLE `ext_dept` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `dept_desc` varchar(500) DEFAULT NULL COMMENT '部门职能描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_dept
-- ----------------------------
INSERT INTO `ext_dept` VALUES ('9986c2aec313412a85e1a01038e430c5', '产品部', '产品开发和维护');
INSERT INTO `ext_dept` VALUES ('abb039b4e6ae4b8b8f5603743f3cf6ae', '技术部', '技术部门描述');
INSERT INTO `ext_dept` VALUES ('de227cd134b0471ebf8f1a774f0b5111', '运维组', '专业维护');
INSERT INTO `ext_dept` VALUES ('ed95178e03a34f139d378f5453eef5d4', '信息中心', '描述');

-- ----------------------------
-- Table structure for `ext_log`
-- ----------------------------
DROP TABLE IF EXISTS `ext_log`;
CREATE TABLE `ext_log` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_name` varchar(300) DEFAULT NULL COMMENT '日志标题',
  `log_url` varchar(500) DEFAULT NULL COMMENT '日志请求地址',
  `log_params` varchar(500) DEFAULT NULL COMMENT '日志参数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_log
-- ----------------------------
INSERT INTO `ext_log` VALUES ('1', 'zhsangan', '/user/add', '{name:zhangsan}', '2016-06-10 01:05:14');
INSERT INTO `ext_log` VALUES ('2', 'list', '/user/del', '{name:zhangsan}', '2016-06-10 01:05:14');

-- ----------------------------
-- Table structure for `ext_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ext_menu`;
CREATE TABLE `ext_menu` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `text` varchar(50) DEFAULT NULL COMMENT '菜单标题',
  `leaf` char(1) DEFAULT '0' COMMENT '是否是叶子节点,0-不是,1-是',
  `pid` varchar(50) DEFAULT '0' COMMENT '父节点ID',
  `view_type` varchar(255) DEFAULT NULL COMMENT '渲染界面',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `icon_cls` varchar(100) DEFAULT NULL,
  `row_cls` varchar(100) DEFAULT NULL,
  `deep` char(11) DEFAULT '1' COMMENT '深度',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `action` varchar(255) DEFAULT '#' COMMENT '资源路径',
  `status` char(1) DEFAULT '1' COMMENT '1-启用,0-禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_menu
-- ----------------------------
INSERT INTO `ext_menu` VALUES ('020d9e228c0148ec808f6360153df917', '统计', '1', '0', 'chart', '2016-05-31 18:29:24', 'x-fa fa-line-chart', 'nav-tree-badge nav-tree-badge-new', '1', '8', null, '1');
INSERT INTO `ext_menu` VALUES ('05a8e26fd0a047eea8fe796587ad6dd0', '新增部门', '1', 'b467700c293a4a549206b5438b93a771', null, '2016-06-27 10:27:03', null, null, '3', '1', '/dept/add', '1');
INSERT INTO `ext_menu` VALUES ('0d7e4107e7e74ed28bb0ee5f1d9641ff', 'EXT基础表单', '1', 'f18713ab6e9d49b5ae2faed0e8f60e45', 'widget-form', '2016-07-09 15:24:18', 'x-fa fa-tasks', null, '2', '0', null, '1');
INSERT INTO `ext_menu` VALUES ('10da6b9f2dc144ff9e4e05aca9de1c6a', '系统日志', '0', '36634b3a36ae4ef0ac566432c98ed7a7', 'log-view', '2016-05-30 20:45:04', 'x-fa fa-info', null, '2', '4', null, '1');
INSERT INTO `ext_menu` VALUES ('13581421dc3c4ab0a9d9464a52511a69', '菜单列表', '1', 'a98a684b73e24174b1718dd6495ad4b0', null, '2016-07-11 10:52:53', null, null, '3', '0', '/menu/list', '1');
INSERT INTO `ext_menu` VALUES ('1f734e10322e41929b04f8660f372fcc', '新增菜单', '1', 'a98a684b73e24174b1718dd6495ad4b0', null, '2016-07-11 10:53:30', null, null, '3', '1', '/menu/add', '1');
INSERT INTO `ext_menu` VALUES ('220b1e3fa78b4a7db0d213dc56700837', '日志查询', '1', '10da6b9f2dc144ff9e4e05aca9de1c6a', null, '2016-06-02 08:54:01', null, null, '3', '0', '/log/list', '1');
INSERT INTO `ext_menu` VALUES ('24f4bfc473d8438faa06de8369c7ab92', '用户管理', '0', '36634b3a36ae4ef0ac566432c98ed7a7', 'user-view', '2016-05-30 20:43:58', 'x-fa fa-user', null, '2', '0', null, '1');
INSERT INTO `ext_menu` VALUES ('36634b3a36ae4ef0ac566432c98ed7a7', '系统管理', '0', '0', null, '2016-05-30 20:44:00', 'x-fa fa-gears', null, '1', '1', null, '1');
INSERT INTO `ext_menu` VALUES ('40b9f6dfed2543f9a917e6846df9ce6a', 'Kindeditor编辑器', '1', 'f18713ab6e9d49b5ae2faed0e8f60e45', 'widget-kindeditor', '2016-07-09 15:59:43', 'x-fa fa-edit', null, '2', '1', null, '1');
INSERT INTO `ext_menu` VALUES ('4aaa67985ebd423196e70f2a973ecc5d', '编辑部门', '1', 'b467700c293a4a549206b5438b93a771', null, '2016-06-27 10:27:39', null, null, '3', '2', '/dept/edit', '1');
INSERT INTO `ext_menu` VALUES ('4c767b293512465d8cf6b677ad520d82', '编辑菜单', '1', 'a98a684b73e24174b1718dd6495ad4b0', null, '2016-07-11 10:53:57', null, null, '3', '2', '/menu/edit', '1');
INSERT INTO `ext_menu` VALUES ('61e9f8e07e3449969fe7a9f849bac30a', '角色管理', '0', '36634b3a36ae4ef0ac566432c98ed7a7', 'role-view', '2016-05-30 20:43:58', 'x-fa fa-users', null, '2', '1', null, '1');
INSERT INTO `ext_menu` VALUES ('63e1cca5fae4488182080eb3f5e4c1c8', '删除角色', '1', '61e9f8e07e3449969fe7a9f849bac30a', null, '2016-06-27 10:16:35', null, null, '3', '3', '/role/delete', '1');
INSERT INTO `ext_menu` VALUES ('6a287b7360e6402580a4c90977c45bdc', '主页', '1', '0', 'dashboard', '2016-05-30 20:41:15', 'x-fa fa-desktop', 'nav-tree-badge nav-tree-badge-hot', '1', '0', null, '1');
INSERT INTO `ext_menu` VALUES ('787a3559dca54c2b896c06b4d17f64fa', '重置密码', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-06-27 09:42:41', null, null, '3', '5', '/user/resetpwd', '1');
INSERT INTO `ext_menu` VALUES ('78a4bb7927c5459ba21af076465014a6', '编辑用户', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-05-30 20:43:58', null, null, '3', '2', '/user/edit', '1');
INSERT INTO `ext_menu` VALUES ('8cad5b44cc644edead242431c644f499', '删除部门', '1', 'b467700c293a4a549206b5438b93a771', null, '2016-06-27 10:28:02', null, null, '3', '3', '/dept/delete', '1');
INSERT INTO `ext_menu` VALUES ('93c6613b01fc4ca3abb81482078d1c3f', '删除菜单', '1', 'a98a684b73e24174b1718dd6495ad4b0', null, '2016-07-11 10:54:22', null, null, '3', '3', '/menu/delete', '1');
INSERT INTO `ext_menu` VALUES ('a0bda115335f4464bf34c75ef96c18af', '编辑角色', '1', '61e9f8e07e3449969fe7a9f849bac30a', null, '2016-06-27 10:16:12', null, null, '3', '2', '/role/edit', '1');
INSERT INTO `ext_menu` VALUES ('a98a684b73e24174b1718dd6495ad4b0', '菜单&资源', '0', '36634b3a36ae4ef0ac566432c98ed7a7', 'tree-menu', '2016-05-30 20:43:58', 'x-fa fa-navicon', null, '2', '3', null, '1');
INSERT INTO `ext_menu` VALUES ('b157d8a09742472bb02cc48ea784b18c', '删除用户', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-06-08 13:07:30', null, null, '3', '4', '/user/delete', '1');
INSERT INTO `ext_menu` VALUES ('b467700c293a4a549206b5438b93a771', '部门管理', '0', '36634b3a36ae4ef0ac566432c98ed7a7', 'dept-view', '2016-06-16 16:56:56', 'x-fa fa-graduation-cap', null, '2', '2', null, '1');
INSERT INTO `ext_menu` VALUES ('c089ed68fe6b47da85e5cb1abb6ec24d', '创建角色', '1', '61e9f8e07e3449969fe7a9f849bac30a', null, '2016-06-27 10:15:52', null, null, '3', '1', '/role/add', '1');
INSERT INTO `ext_menu` VALUES ('c948820b5cd040109567ee5a22369f95', '修改密码', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-07-11 11:56:34', null, null, '3', '6', '/user/updatepwd', '1');
INSERT INTO `ext_menu` VALUES ('d08d362db39e43269fbd7f4ae6a464d0', '部门列表', '1', 'b467700c293a4a549206b5438b93a771', null, '2016-06-27 10:26:45', null, null, '3', '0', '/dept/list', '1');
INSERT INTO `ext_menu` VALUES ('d442ead2915444eea92133d36dcf0a7a', '分配权限', '1', '61e9f8e07e3449969fe7a9f849bac30a', null, '2016-06-27 10:17:26', null, null, '3', '2', '/role/doAuth', '1');
INSERT INTO `ext_menu` VALUES ('d557c1f9fbe14c169a86186fa8be0bfb', '查询用户', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-06-02 09:37:12', null, null, '3', '0', '/user/list', '1');
INSERT INTO `ext_menu` VALUES ('df2defffefb7484bb07f0fc31543be4e', '新增用户', '1', '24f4bfc473d8438faa06de8369c7ab92', null, '2016-05-30 20:43:58', null, null, '3', '1', '/user/add', '1');
INSERT INTO `ext_menu` VALUES ('e396382bd397462596ae1a7f6bf3efc1', '角色列表', '1', '61e9f8e07e3449969fe7a9f849bac30a', null, '2016-06-27 10:15:09', null, null, '3', '0', '/role/list', '1');
INSERT INTO `ext_menu` VALUES ('f18713ab6e9d49b5ae2faed0e8f60e45', '组件实例', '0', '0', null, '2016-07-09 15:05:03', 'x-fa fa-list', null, '1', '2', null, '1');

-- ----------------------------
-- Table structure for `ext_role`
-- ----------------------------
DROP TABLE IF EXISTS `ext_role`;
CREATE TABLE `ext_role` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `row_status` char(11) DEFAULT '0' COMMENT '状态,0-启用，1-禁用，2-删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_role
-- ----------------------------
INSERT INTO `ext_role` VALUES ('380dc2536c554eae9655ccc05f16374f', '超级管理员', '2016-06-08 14:40:54', '拥有所有权限', '0');
INSERT INTO `ext_role` VALUES ('6a287b7360e6402580a4c90977c45bdc', '普通管理员', '2016-06-08 21:47:04', '拥有系统基本权限', '2');
INSERT INTO `ext_role` VALUES ('6abdd6fe57694c4b992be3d5a554ca11', '普通管理员', '2016-06-11 11:39:16', '普通管理员', '0');
INSERT INTO `ext_role` VALUES ('e2f26329c49e4e9fbb36fb74a1cedb1e', '查询角色', '2016-07-11 10:50:15', '用于所有查询权限', '0');

-- ----------------------------
-- Table structure for `ext_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ext_role_menu`;
CREATE TABLE `ext_role_menu` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(50) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_role_menu
-- ----------------------------
INSERT INTO `ext_role_menu` VALUES ('02e74b6b95ed41248050cfeeb406642b', '380dc2536c554eae9655ccc05f16374f', '36634b3a36ae4ef0ac566432c98ed7a7');
INSERT INTO `ext_role_menu` VALUES ('06b1e5e7c5e441e0b8171e10e8f2ef58', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '13581421dc3c4ab0a9d9464a52511a69');
INSERT INTO `ext_role_menu` VALUES ('07754613867b43e29b0ab1b25f321679', '380dc2536c554eae9655ccc05f16374f', 'd442ead2915444eea92133d36dcf0a7a');
INSERT INTO `ext_role_menu` VALUES ('1380326242e943a88a2bcd604de2459d', '380dc2536c554eae9655ccc05f16374f', '220b1e3fa78b4a7db0d213dc56700837');
INSERT INTO `ext_role_menu` VALUES ('199954fd2968450e8754af7d9ebb292c', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_role_menu` VALUES ('25407edc9c7b4c46869b48044ab8f18c', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'd08d362db39e43269fbd7f4ae6a464d0');
INSERT INTO `ext_role_menu` VALUES ('2d48ded97bb1447fae541b3308ee4c4d', '380dc2536c554eae9655ccc05f16374f', '40b9f6dfed2543f9a917e6846df9ce6a');
INSERT INTO `ext_role_menu` VALUES ('2dabea4b5b394fd2a6293d9c51a7afcd', '380dc2536c554eae9655ccc05f16374f', 'd557c1f9fbe14c169a86186fa8be0bfb');
INSERT INTO `ext_role_menu` VALUES ('30d77f9f44e64a0c97c95cc618862e2a', '380dc2536c554eae9655ccc05f16374f', 'a98a684b73e24174b1718dd6495ad4b0');
INSERT INTO `ext_role_menu` VALUES ('338cbd0d2d4b43e1afcaba1ae7338da3', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'f18713ab6e9d49b5ae2faed0e8f60e45');
INSERT INTO `ext_role_menu` VALUES ('386ed2fff1604511bb147799fbf9fd0c', '380dc2536c554eae9655ccc05f16374f', 'e396382bd397462596ae1a7f6bf3efc1');
INSERT INTO `ext_role_menu` VALUES ('39afacfcb64b4a3d8dad3c17370707f3', '380dc2536c554eae9655ccc05f16374f', '78a4bb7927c5459ba21af076465014a6');
INSERT INTO `ext_role_menu` VALUES ('3a0a6e20565c46218341f496034ba7e5', '380dc2536c554eae9655ccc05f16374f', 'df2defffefb7484bb07f0fc31543be4e');
INSERT INTO `ext_role_menu` VALUES ('3f1a054041d949d2878a66319629dc06', '380dc2536c554eae9655ccc05f16374f', 'c089ed68fe6b47da85e5cb1abb6ec24d');
INSERT INTO `ext_role_menu` VALUES ('49d6d460ce5a4c1eb575ea0307129594', '380dc2536c554eae9655ccc05f16374f', 'f18713ab6e9d49b5ae2faed0e8f60e45');
INSERT INTO `ext_role_menu` VALUES ('540a55dd557a4aebb8c7be065fde1c5b', '6abdd6fe57694c4b992be3d5a554ca11', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_role_menu` VALUES ('5eeaaadc3bd54e409f9e08e64596f11d', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'a98a684b73e24174b1718dd6495ad4b0');
INSERT INTO `ext_role_menu` VALUES ('5f4718cfd8c0467bbb81fb84111e85e9', '6abdd6fe57694c4b992be3d5a554ca11', '020d9e228c0148ec808f6360153df917');
INSERT INTO `ext_role_menu` VALUES ('6236002dadfe426f8e7b209fe021667e', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '61e9f8e07e3449969fe7a9f849bac30a');
INSERT INTO `ext_role_menu` VALUES ('62fded562ed24d73b0ecbadb17ee59c6', '380dc2536c554eae9655ccc05f16374f', '93c6613b01fc4ca3abb81482078d1c3f');
INSERT INTO `ext_role_menu` VALUES ('649db73a0e354eb7951f3565d961e06d', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '10da6b9f2dc144ff9e4e05aca9de1c6a');
INSERT INTO `ext_role_menu` VALUES ('6559b7fe546141848b2d6505d1e94ed7', '6abdd6fe57694c4b992be3d5a554ca11', '10da6b9f2dc144ff9e4e05aca9de1c6a');
INSERT INTO `ext_role_menu` VALUES ('68c174999b624d988f6284f4cb229678', '380dc2536c554eae9655ccc05f16374f', '020d9e228c0148ec808f6360153df917');
INSERT INTO `ext_role_menu` VALUES ('70a90114710b4b89a922bef0e952389d', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '020d9e228c0148ec808f6360153df917');
INSERT INTO `ext_role_menu` VALUES ('7c4e075871644cc8820eeb393d5c8dca', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '24f4bfc473d8438faa06de8369c7ab92');
INSERT INTO `ext_role_menu` VALUES ('82bae9ca7d3749cda0eed7ec8e01cbb1', '380dc2536c554eae9655ccc05f16374f', '61e9f8e07e3449969fe7a9f849bac30a');
INSERT INTO `ext_role_menu` VALUES ('83d52900621845889b3d3eb5db7840ac', '380dc2536c554eae9655ccc05f16374f', '8cad5b44cc644edead242431c644f499');
INSERT INTO `ext_role_menu` VALUES ('950a09f4d5d44c5abdcf845ae6d3543a', '6abdd6fe57694c4b992be3d5a554ca11', '36634b3a36ae4ef0ac566432c98ed7a7');
INSERT INTO `ext_role_menu` VALUES ('9b1df6fe387449c0814e25f43d51d80b', '380dc2536c554eae9655ccc05f16374f', '4aaa67985ebd423196e70f2a973ecc5d');
INSERT INTO `ext_role_menu` VALUES ('9d26eb63e94b4870b0524b9fc644f431', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '0d7e4107e7e74ed28bb0ee5f1d9641ff');
INSERT INTO `ext_role_menu` VALUES ('a22b9cfa5b9b4fb488f9cdd7d1f2a1fd', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '36634b3a36ae4ef0ac566432c98ed7a7');
INSERT INTO `ext_role_menu` VALUES ('a533eac06c6740ef96ac3db5fcee3ad6', '380dc2536c554eae9655ccc05f16374f', 'c948820b5cd040109567ee5a22369f95');
INSERT INTO `ext_role_menu` VALUES ('a6ab7e040081461c9c624cc4cf15a620', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'd557c1f9fbe14c169a86186fa8be0bfb');
INSERT INTO `ext_role_menu` VALUES ('aa1c3c1f00de442e93fa17484eefbdd5', '380dc2536c554eae9655ccc05f16374f', '13581421dc3c4ab0a9d9464a52511a69');
INSERT INTO `ext_role_menu` VALUES ('abcbc150a1aa407ba6cc795b356ffddf', '380dc2536c554eae9655ccc05f16374f', '63e1cca5fae4488182080eb3f5e4c1c8');
INSERT INTO `ext_role_menu` VALUES ('b238afc02a5b415cb5b89e4b62010d1a', '380dc2536c554eae9655ccc05f16374f', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_role_menu` VALUES ('b42f2a203e4c4924b40f3adc98ad52db', '380dc2536c554eae9655ccc05f16374f', 'b157d8a09742472bb02cc48ea784b18c');
INSERT INTO `ext_role_menu` VALUES ('c06cedef6d704b2bb1ca2952c29b9075', '380dc2536c554eae9655ccc05f16374f', '4c767b293512465d8cf6b677ad520d82');
INSERT INTO `ext_role_menu` VALUES ('c3cd488a5dbc423e89fe131505743927', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '40b9f6dfed2543f9a917e6846df9ce6a');
INSERT INTO `ext_role_menu` VALUES ('cc8d3fff756444c2b634a52cf41ea925', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'b467700c293a4a549206b5438b93a771');
INSERT INTO `ext_role_menu` VALUES ('d280dba56ca04ce19a0c16eae09a6a80', '380dc2536c554eae9655ccc05f16374f', '0d7e4107e7e74ed28bb0ee5f1d9641ff');
INSERT INTO `ext_role_menu` VALUES ('da1c583d8b54465d94d55b4e24647a2e', '380dc2536c554eae9655ccc05f16374f', '787a3559dca54c2b896c06b4d17f64fa');
INSERT INTO `ext_role_menu` VALUES ('e448a59cd2ea43e6b56eaf4087fe078f', '380dc2536c554eae9655ccc05f16374f', '1f734e10322e41929b04f8660f372fcc');
INSERT INTO `ext_role_menu` VALUES ('e4eaf8c67be6450a96c279f9a59f01cb', '380dc2536c554eae9655ccc05f16374f', 'd08d362db39e43269fbd7f4ae6a464d0');
INSERT INTO `ext_role_menu` VALUES ('e8c191d60d7c4f0390e68f5cb332d377', 'e2f26329c49e4e9fbb36fb74a1cedb1e', '220b1e3fa78b4a7db0d213dc56700837');
INSERT INTO `ext_role_menu` VALUES ('ec3692dfbbe74055ba4ab795020c5621', '380dc2536c554eae9655ccc05f16374f', '05a8e26fd0a047eea8fe796587ad6dd0');
INSERT INTO `ext_role_menu` VALUES ('f1fbfe72884d439dbf8f81a4d40013eb', 'e2f26329c49e4e9fbb36fb74a1cedb1e', 'e396382bd397462596ae1a7f6bf3efc1');
INSERT INTO `ext_role_menu` VALUES ('f5d56366d13142ddbf8e622b4aa8e52c', '380dc2536c554eae9655ccc05f16374f', '24f4bfc473d8438faa06de8369c7ab92');
INSERT INTO `ext_role_menu` VALUES ('f9c4e6875a884794acd0931342dc6d0a', '6abdd6fe57694c4b992be3d5a554ca11', '220b1e3fa78b4a7db0d213dc56700837');
INSERT INTO `ext_role_menu` VALUES ('fa028e73ed4f4adea7a273b72e1af274', '380dc2536c554eae9655ccc05f16374f', 'b467700c293a4a549206b5438b93a771');
INSERT INTO `ext_role_menu` VALUES ('ff658aec62db464fba5c5162395d164d', '380dc2536c554eae9655ccc05f16374f', 'a0bda115335f4464bf34c75ef96c18af');
INSERT INTO `ext_role_menu` VALUES ('ffd1792dd4544158a0c9975b68f1d03d', '380dc2536c554eae9655ccc05f16374f', '10da6b9f2dc144ff9e4e05aca9de1c6a');

-- ----------------------------
-- Table structure for `ext_user`
-- ----------------------------
DROP TABLE IF EXISTS `ext_user`;
CREATE TABLE `ext_user` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户登录ID',
  `user_pwd` varchar(50) DEFAULT NULL COMMENT '用户密码',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `user_email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `row_status` char(1) DEFAULT '0' COMMENT '状态,0-启用，1-禁用，2-删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `user_desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `dept_id` varchar(50) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_user
-- ----------------------------
INSERT INTO `ext_user` VALUES ('805c8fdd191d45b88aed43b2614260ea', 'test', 'E10ADC3949BA59ABBE56E057F20F883E', 'Test', '111111111@qq.com', '0', '2016-07-11 10:49:53', '工作要肯拼,生活要欢乐！', '9986c2aec313412a85e1a01038e430c5');
INSERT INTO `ext_user` VALUES ('b4e6e5259b7b4d56aafcd6b3073771db', 'zhangsan', 'E10ADC3949BA59ABBE56E057F20F883E', '张三', 'zhangsan@vaco.com', '2', '2016-06-08 20:55:36', 'uuuuu', 'abb039b4e6ae4b8b8f5603743f3cf6ae');
INSERT INTO `ext_user` VALUES ('fa72ed734b7b48e1b09ceba1ffadb724', 'admin', 'E10ADC3949BA59ABBE56E057F20F883E', 'Admin管理员', 'admin@qq.com', '0', '2016-06-08 18:13:25', '工作要肯拼，生活要欢乐。', '9986c2aec313412a85e1a01038e430c5');

-- ----------------------------
-- Table structure for `ext_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `ext_user_role`;
CREATE TABLE `ext_user_role` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户主键',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ext_user_role
-- ----------------------------
INSERT INTO `ext_user_role` VALUES ('0c555ffcf05645019ef27a6fb37cc77f', 'a84a2f53682645cd93adbc96c0c33f1e', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_user_role` VALUES ('466b75401408467fa01e40b944e9e317', 'b4e6e5259b7b4d56aafcd6b3073771db', '6abdd6fe57694c4b992be3d5a554ca11');
INSERT INTO `ext_user_role` VALUES ('87ec121412ab4a0cabd941228f498ddc', 'c46f6c7913374ade8565d9d412a385c1', '6abdd6fe57694c4b992be3d5a554ca11');
INSERT INTO `ext_user_role` VALUES ('8da5a65835c346e3a2b8015780a88189', '25b29869ef2d40f5a3ce8eaa589fea35', '6abdd6fe57694c4b992be3d5a554ca11');
INSERT INTO `ext_user_role` VALUES ('99dd910cfa9e4142acefdae9bd98d2fc', '7b7c4d46de6744f99a0031ed01f4346f', '380dc2536c554eae9655ccc05f16374f');
INSERT INTO `ext_user_role` VALUES ('a039cd21a8494e3a9bfbe6ffa81d9140', 'c46f6c7913374ade8565d9d412a385c1', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_user_role` VALUES ('a8bb350d28d547c597ba5c36dc3551ad', '251b866b0b3e45b0b3979c02318289f7', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_user_role` VALUES ('b8dc64d8f1634c8d9fc373cde52ad7e0', '25b29869ef2d40f5a3ce8eaa589fea35', '6a287b7360e6402580a4c90977c45bdc');
INSERT INTO `ext_user_role` VALUES ('bbd3919dc9ec40618f5316c21627a6d4', '805c8fdd191d45b88aed43b2614260ea', 'e2f26329c49e4e9fbb36fb74a1cedb1e');
INSERT INTO `ext_user_role` VALUES ('dfd0986319e94742966f7c506083f4ff', '34ddcbdcbe3c4b15b554680f176ad587', '6abdd6fe57694c4b992be3d5a554ca11');
INSERT INTO `ext_user_role` VALUES ('fb058462cf6147ac9598e1cebbaa943a', 'fa72ed734b7b48e1b09ceba1ffadb724', '380dc2536c554eae9655ccc05f16374f');
